<?php

namespace Database\Seeders;

use App\Models\Character;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CharacterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Character::factory(130)->create();
        foreach (Character::all() as $row) {
            $row->url = 'http://localhost:7000/api/crud/characters/'.$row->id;
            $row->save();
            $range = range(1, 30);
            shuffle($range);
            $arr = array_slice($range,0,4);
            $row->episodes()->attach($arr);
        }
    }
}
