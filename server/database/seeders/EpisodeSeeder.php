<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Episode;
class EpisodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Episode::factory(30)->create();
        foreach (Episode::all() as $row) {
            $row->url = 'http://localhost:7000/api/episodes/'.$row->id;
            $row->save();
        }
    }
}
