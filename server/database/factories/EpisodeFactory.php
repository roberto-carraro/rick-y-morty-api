<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EpisodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(),
            'created' => $this->faker->dateTime(),
            'air_date' => $this->faker->date('F d, Y'),
            'episode' => 'S'.$this->faker->numberBetween(1,10).'E'.$this->faker->numberBetween(1,20),
        ];
    }
}
