<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->name(),
            'image' => 'https://rickandmortyapi.com/api/character/avatar/'.$this->faker->numberBetween(1,100).'.jpeg',
            'created' => $this->faker->dateTime(),
            'gender' => $this->faker->randomElement(['Male' ,'Female']),
            'status' => $this->faker->randomElement(['Alive' ,'unknown', 'Dead']),
            'species' => $this->faker->randomElement(['Alien' ,'Human']),
            'location_id' => $this->faker->numberBetween(1,30),
            'origin_id' => $this->faker->numberBetween(1,30),
        ];
    }
}
