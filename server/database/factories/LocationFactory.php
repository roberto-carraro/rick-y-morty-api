<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LocationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->city(),
            'created' => $this->faker->dateTime(),
            'dimension' => 'Dimension C-137',
            'type' => $this->faker->randomElement(['Microverse' ,'Cluster', 'Space station', 'Planet', 'Microverse']),
        ];
    }
}
