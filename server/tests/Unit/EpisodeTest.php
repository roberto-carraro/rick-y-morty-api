<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Episode;

class EpisodeTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_of_episodes_can_be_retrieved()
    {
        $this->withoutExceptionHandling();
        Episode::factory(3)->create();
        $response = $this->get('/api/episodes');
        $response->assertOk();
    }
}
