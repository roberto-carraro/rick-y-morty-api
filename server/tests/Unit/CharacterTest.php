<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Character;
use App\Models\Location;
class CharacterTest extends TestCase
{

    use RefreshDatabase;

    public function test_list_of_characters_can_be_retrieved()
    {
        $this->withoutExceptionHandling();
        Location::factory(30)->create();
        Character::factory(3)->create();
        $response = $this->get('/api/crud/characters');
        $response->assertOk();
    }

    public function test_a_character_can_be_created()
    {
        $this->withoutExceptionHandling();
        $response = $this->post('/api/crud/characters', [
            'name' => 'Roberto Carraro',
            'image' => 'https://rickandmortyapi.com/api/character/avatar/19.jpeg',
            'gender' => 'Female',
            'status' => 'Alive',
            'species' => 'Alien',
        ]);
        $response->assertOk();
        $this->assertCount(1, Character::all());
        $character = Character::first();
        $this->assertEquals($character->name, 'Roberto Carraro');
        $this->assertEquals($character->image, 'https://rickandmortyapi.com/api/character/avatar/19.jpeg');
        $this->assertEquals($character->gender, 'Female');
        $this->assertEquals($character->status, 'Alive');
        $this->assertEquals($character->species, 'Alien');
    }

    public function test_a_characters_can_be_retrieved()
    {
        $this->withoutExceptionHandling();
        Location::factory(30)->create();
        $character = Character::factory(1)->create()[0];
        $response = $this->get('/api/crud/characters/' . $character->id);
        $response->assertOk();
    }

    public function test_a_character_can_be_updated()
    {
        $this->withoutExceptionHandling();
        Location::factory(30)->create();
        $character = Character::factory(1)->create()[0];
        $response = $this->patch('/api/crud/characters?id=' . $character->id, [
            'name' => 'Roberto Carraros',
            'image' => '...',
        ]);
        $character = $character->fresh();
        $response->assertOk();
        $this->assertEquals($character->name, 'Roberto Carraros');
        $this->assertEquals($character->image, '...');
        
    }

    public function test_a_character_can_be_deleted()
    {
        $this->withoutExceptionHandling();
        Location::factory(30)->create();
        $character = Character::factory(1)->create()[0];
        $response = $this->delete('/api/crud/characters?id=' . $character->id);
        $response->assertOk();
        $this->assertCount(0, Character::all());
    }
}
