<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Location;

class LocationTest extends TestCase
{
    use RefreshDatabase;

    public function test_list_of_locations_can_be_retrieved()
    {
        $this->withoutExceptionHandling();
        Location::factory(3)->create();
        $response = $this->get('/api/locations');
        $response->assertOk();
    }
}
