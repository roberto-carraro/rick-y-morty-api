<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\EpisodeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Healthcheck
 *
 * Check that the service is up. If everything is okay, you'll get a 200 OK response.
 *
 * Otherwise, the request will fail with a 400 error, and a response listing the failed services.
 *
 * @response 400 scenario="Service is unhealthy" {"status": "down", "services": {"database": "up", "redis": "down"}}
 * @responseField status The status of this API (`up` or `down`).
 * @responseField services Map of each downstream service and their status (`up` or `down`).
 */
Route::resource('locations', LocationController::class)->only('index', 'show');
Route::resource('episodes', EpisodeController::class)->only('index', 'show');

Route::delete('crud/characters', [CharacterController::class, 'destroy']);
Route::patch('crud/characters', [CharacterController::class, 'update']);
Route::resource('crud/characters', CharacterController::class)->only('index', 'store', 'show');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
