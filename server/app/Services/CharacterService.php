<?php

namespace App\Services;

use App\Http\Audit\CharacterAudit;
use App\Models\Character;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
class CharacterService
{
    
    public function create(array $data): Character
    {   
        //$data['image'] = isset($data['image']) ? $data['image'] : 'https://rickandmortyapi.com/api/character/avatar/19.jpeg';
        $data['image'] = $this->saveImage($data['image'] ?? null);
        //$data['location'] = $data['location'] ?? "";
        
        $now = Carbon::now();
        $data['created'] = $now;
        $character = Character::create($data);
        return $character;
    }
    public function update(array $data, $id): Character
    {   
        $data['image'] = $this->saveImage($data['image']);
        $character = Character::find($id);
        $character->update($data);
        return $character;
    }
    
    protected function saveImage($data) {
        if(
            isset($data) &&
            substr($data, 0, 5) == 'data:' 
            //$data != 'https://rickandmortyapi.com/api/character/avatar/19.jpeg'
        ) {
            $arr = explode('base64,', $data);
            $extension = explode('/', mime_content_type($data))[1];
            $path = 'images/'.uniqid().'.'.$extension;
            Storage::disk('public')->put($path, base64_decode($arr[1]));
            return Storage::disk('public')->url($path);
        }
        return $data;
    }
}
