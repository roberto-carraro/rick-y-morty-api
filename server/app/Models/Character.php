<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Character extends BaseModel
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'image',
        'created',
        'gender',
        'type',
        'location_id',
        'origin_id',
        'species',
        'status',
        'url',
    ];
    protected $appends = [
        'created_at_diff',
        'updated_at_diff',
    ];
    public function episodes()
    {
        return $this->belongsToMany(Episode::class);
    }
    public function rawLocation()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }
    public function rawOrigin()
    {
        return $this->belongsTo(Location::class, 'origin_id');
    }
    public function getCreatedAtDiffAttribute()
    {
        return Carbon::parse($this->created_at)->locale('es')->diffForHumans();
    }
    
    public function getUpdatedAtDiffAttribute()
    {
        return Carbon::parse($this->updated_at)->locale('es')->diffForHumans();
    }
    // public function getLocationAttribute()
    // {
    //     return [
    //         'name' => $this->rawLocation->name,
    //         'url' => $this->rawLocation->url
    //     ];
    // }
    // public function getOriginAttribute()
    // {
    //     return [
    //         'name' => $this->place->name,
    //         'url' => $this->place->url
    //     ];
    // }
    // public function getEpisodeAttribute()
    // {
    //     $arr = [];
    //     foreach ($this->episodes as $item) {
    //        $arr[] = $item->url;
    //     }
    //     return $arr;
    // }
}
