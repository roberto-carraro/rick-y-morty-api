<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
class BaseModel extends Model
{
    use SoftDeletes;

    protected static function booted()
    {
        static::creating(function ($model) {

            $user = Auth::user();

            if ($user) {
                $model->forceFill([
                    'created_by' => $user->id,
                    'updated_by' => $user->id,
                ]);
            }
        });

        static::updating(function ($model) {
            $user = Auth::user();
            if ($user) {
                $model->forceFill([
                    'updated_by' => $user->id,
                ]);
            }
        });
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

}
