<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Episode extends BaseModel
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'url',
        'created',
        'air_date',
        'episode',
    ];
    protected $appends = [
        'created_at_diff',
        'updated_at_diff',
        'characters'
    ];
    public function personages()
    {
        return $this->belongsToMany(Character::class);
    }
    public function getCreatedAtDiffAttribute()
    {
        return Carbon::parse($this->created_at)->locale('es')->diffForHumans();
    }
    
    public function getUpdatedAtDiffAttribute()
    {
        return Carbon::parse($this->updated_at)->locale('es')->diffForHumans();
    }
    public function getCharactersAttribute()
    {
        $arr = [];
        foreach ($this->personages as $item) {
           $arr[] = $item->url;
        }
        return $arr;
    }
}
