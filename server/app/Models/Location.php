<?php

namespace App\Models;

use App\Models\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Location extends BaseModel
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'url',
        'created',
        'dimension',
        'type',
    ];
    protected $appends = [
        'created_at_diff',
        'updated_at_diff',
        'residents'
    ];
    public function characters()
    {
        return $this->hasMany(Character::class);
    }
    public function getCreatedAtDiffAttribute()
    {
        return Carbon::parse($this->created_at)->locale('es')->diffForHumans();
    }
    
    public function getUpdatedAtDiffAttribute()
    {
        return Carbon::parse($this->updated_at)->locale('es')->diffForHumans();
    }

    public function getResidentsAttribute()
    {
        $arr = [];
        foreach ($this->characters as $item) {
           $arr[] = $item->url;
        }
        return $arr;
    }
}
