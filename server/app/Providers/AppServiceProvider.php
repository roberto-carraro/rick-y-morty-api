<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Resources\CharacterResource;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        /**
         * Quito llave data de las respuestas 
         */
        CharacterResource::withoutWrapping();
    }
}
