<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCharacterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'nullable',
            ],
            'image' => ['nullable'],
            'created' => ['nullable'],
            'gender' => ['nullable'],
            'type' => ['nullable'],
            'location_id' => ['nullable'],
            'origin_id' => ['nullable'],
            'species' => ['nullable'],
            'status' => ['nullable'],
            'url' => ['nullable'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
        ];
    }
}
