<?php

namespace App\Http\Resources;
use App\Models\Character;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CharacterResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * ciel -> busca el entero mas alto
         */
        return [
            'results' => $this->collection->map(function ($item) {
                $arr = [];
                foreach ($item->episodes as $episode) {
                    $arr[] = $episode->url;
                }
                $item->episode = $arr;
                if(isset($item->rawLocation)) {
                    $item->location = [
                        'name' => $item->rawLocation->name,
                        'url' => $item->rawLocation->url
                    ];
                } else {
                    $item->location = "";
                }
                if(isset($item->rawOrigin)) {
                    $item->origin = [
                        'name' => $item->rawOrigin->name,
                        'url' => $item->rawOrigin->url
                    ];
                }
                $item = (object) $item->toArray();
                $arr = [];
                foreach($item as $key=>$value) {
                    $arr[$key] = $value ?? "";
                }
                return $arr;
            }),
            'info' => [
                'count' => Character::all()->count(),
                'pages' => ceil(Character::all()->count() / 20)
            ],
        ];
    }
}
