<?php

namespace App\Http\Resources;
use App\Models\Episode;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EpisodeResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * ciel -> busca el entero mas alto
         */
        return [
            'results' => $this->collection,
            'info' => [
                'count' => Episode::all()->count(),
                'pages' => ceil(Episode::all()->count() / 20)
            ],
        ];
    }
}
