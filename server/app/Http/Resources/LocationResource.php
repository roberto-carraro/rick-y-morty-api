<?php

namespace App\Http\Resources;
use App\Models\Location;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LocationResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        /**
         * ciel -> busca el entero mas alto
         */
        return [
            'results' => $this->collection,
            'info' => [
                'count' => Location::all()->count(),
                'pages' => ceil(Location::all()->count() / 20)
            ],
        ];
    }
}
