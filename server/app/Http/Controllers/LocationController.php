<?php

namespace App\Http\Controllers;

use App\Http\Resources\LocationResource;
use App\Models\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    
    public function index(Request $request)
    {
        $limit = 20;
        $offset = ($request->page - 1) * $limit;
        $locations = Location::offset($offset)
            ->limit($limit)
            ->with('characters')
            ->get();
        return new LocationResource($locations);
    }
    
    public function show(Location $location)
    {
        $response = [];
        $response['error'] = false;
        $response['data'] = $location;
        return response()->json($response);
    }
}
