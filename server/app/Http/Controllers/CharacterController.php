<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCharacterRequest;
use App\Http\Requests\UpdateCharacterRequest;
use App\Http\Resources\CharacterResource;
use App\Models\Character;
use App\Services\CharacterService;
use Illuminate\Http\Request;
use App\Http\Resources\CommonResource;

class CharacterController extends Controller
{
    
    protected $service;
    public function __construct(
        CharacterService $service,
    ) {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $limit = 20;
        $offset = ($request->page - 1) * $limit;
        $characters = Character::offset($offset)
            ->limit($limit)
            //->with('episodes')
            ->get();
       
        return new CharacterResource($characters);
        
    }

    public function store(StoreCharacterRequest $request)
    {
        $data = $request->validated();
        $character = $this->service->create($data);
        $response = [];
        $response['error'] = false;
        $response['msg'] = "Registro creado";
        return response()->json($response);
    }

    public function show(Character $character)
    {
        $response = [];
        $response['error'] = false;
        $response['data'] = $character;
        return response()->json($response);
    }
    public function update(UpdateCharacterRequest $request)
    {
        $validated = $request->validated();
        $character = $this->service->update($validated, $request->id);
        
        $response = [];
        $response['error'] = false;
        $response['msg'] = "Registro editado";
        return response()->json($response);
        
    }
    
    public function destroy(Request $request)
    {
        $character = Character::find($request->id);
        $character->delete();
        $response = [];
        $response['error'] = false;
        $response['msg'] = "Registro eliminado";
        return response()->json($response);
    }

}
