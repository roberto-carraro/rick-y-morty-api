<?php

namespace App\Http\Controllers;

use App\Http\Resources\EpisodeResource;
use App\Models\Episode;
use Illuminate\Http\Request;

class EpisodeController extends Controller
{
    
    public function index(Request $request)
    {
        $limit = 20;
        $offset = ($request->page - 1) * $limit;
        $episodes = Episode::offset($offset)
            ->limit($limit)
            ->get();
        return new EpisodeResource($episodes);
    }
    
    public function show(Episode $episode)
    {
        $response = [];
        $response['error'] = false;
        $response['data'] = $episode;
        return response()->json($response);
    }
}
