# Challenge - API de Rick & Morty

Se desarrolló una REST API con el framework de php Laravel 8.  

## FEATURES
- CRUD (Create, read, update y delete) de la entidad characters.
- Endpoint para listar lugares (locations).
- Endpoint para visualizar un lugar (location).
- Endpoint para listar episodios (episodes).
- Endpoint para visualizar un episodio (episode).
- Pruebas unitarias de todos los endpoints (characters, locations y episodes).
- Correccion de issues en el cliente


## INSTALACION

Correr en la consola

```
git clone https://gitlab.com/roberto-carraro/rick-y-morty-api.git
```

Ir raiz del proyecto
```
cd rick-y-morty-api
```

- ### FRONTEND
    1. Ir a la carpeta del cliente
        ```
        cd client
        ```

    2. Instalar dependencias
        ```
        npm install
        ```
    
    3. Correr el cliente
        ```
        npm run start:dev
        ```


- ### BACKEND
    
    1. Ir a la carpeta del servidor
            ```
            cd server
            ```
    2. Renombrar el archivo .env.example por .env ubicado en la raiz del server
    
    3. Crear un archivo database.sqlite en server/database

    4. Instalación de paquetes
        ```
        composer install
        ```
    
    5. Correr las migraciones
        ```
        php artisan migrate
        ```

    6. Correr las pruebas unitarias (Unit tests)
        ```
        php artisan test
        ```
    
    7. Crear enlace simbólico
        ```
        php artisan storage:link
        ```
    
    8. Insertar datos de prueba
        ```
        php artisan db:seed
        ```

    9. Correr el servidor
        ```
        php artisan serve --port 7000
        ```

## COMPILACIÓN FRONTEND
- Para compilar React (cuando haya sido modificado) se debe ejecutar npm run build en ./client


## ISSUES CORREGIDAS (Cliente)
- Al Abrir por primera vez el detalle de una entidad sus datos persisten en las subsiguientes, al hacer F5 y consultar otra entidad por primera vez, son estos nuevos datos los que persisten

    Para solucionarlo se agregó <code>key={value}</code> en ./client/src/fragments/CharacterModalContent.js

    ```
    <div className='card'>
                                                
        {editing ? (
            
            <input
                name={k}
                type={inputType}
                className='form-control'
                key={value}
                defaultValue={value}
            />
        ) : (
                <div className='ReadonlyControl card-text p-1 pt-2 pb-2'>
                    {value}
                </div>
            )}
    </div>
    ```

- Al crear un nuevo personaje y cerrar el diálogo modal no se está refrescando la vista del listado general.

    Para solucionarlo se agregó 
    <code>
        let cardsGrid = document.getElementById('cards-grid');
        setTimeout(() => {
            loadCharactersCRUD(info.pages);
            goToBottom(cardsGrid);
        }, 100);
    </code> 
    en ./client/src/pages/CharactersPage.js

    ```
    async function onSaveHandle(isNew){
        ...

        if(isNew){
            CRUD.create('characters', {data: objectData});
            
            let cardsGrid = document.getElementById('cards-grid');
            setTimeout(() => {
                loadCharactersCRUD(info.pages);
                goToBottom(cardsGrid);
            }, 100);
        } 
        ...
    }
    function goToBottom(element) {
        element.scroll({ top: element.scrollHeight, behavior: "smooth"})
    }
    ```
    Cuando se crea un nuevo personaje el modal se cierra y la navegación va al última página y el contenedor del listado se scrollea hasta final para que se pueda visualizar el elemento creado. 

- Al editar un personaje y cerrar el diálogo modal no se está refrescando la vista del listado general.

    Para solucionarlo se agregó 
    <code>
        setTimeout(() => {
            loadCharactersCRUD(page);
        }, 100);
    </code> 
    en ./client/src/pages/CharactersPage.js

    ```
    async function onSaveHandle(isNew){
        ...

        if(isNew){
            ...
        } else {
            CRUD.update('characters', {data: objectData});
            setTimeout(() => {
                loadCharactersCRUD(page);
            }, 100);
        }
        ...
    }
    ```

- Ocultar los botones de siguiente y anterior de la paginación cuando la navegación está al última y en la primera página respectivamente.

    Para solucionarlo se agregó 
    <code>
        {
            current > 1 &&  
            ...
        }
    </code> y
    <code>
        {
            current < info.pages &&  
            ...
        }
    </code> y
    en ./client/src/components/Pagination.js
    ```
    return (
        <nav aria-label="Page navigation">
            <ul className="mt-4 pagination pagination-sm justify-content-center">
                {
                    current > 1 &&  
                    <li className="page-item">
                        <a onClick={()=>pagingHandle(current - 1)} className="page-link" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                }
                {
                   ...
                }
                {
                    current < info.pages &&  
                    <li className="page-item">
                        <a onClick={()=>pagingHandle(current + 1)} className="page-link" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                }
            </ul>
        </nav>
    )
    ```

## STACK UTILIZADO
- Php
- SQLite
- Git y npm
- Composer

## AUTOR de la API
- Roberto Carraro (@roberto-carraro)


## FECHA DE ÚLTIMA ACTUALIZACIÓN
25/07/2022

por @roberto-carraro